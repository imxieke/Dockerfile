### Xiekers Docker Images Repo!

```
alpine          ---> Alpine Linux edge
archlinux       ---> Archlinux Base Image
Buildbot        ---> Archlinux aur Build Bot
debian        	---> Debian buster, an operating system and a distribution of Free Software
golang          ---> go language support environment
h5ai            ---> online file view
java            ---> java(8) language support environment.
nextcloud       ---> an open source, self-hosted file share and communication platform, like owncloud.
owncloud        ---> an open source, self-hosted file sync and share app platform.
php7            ---> php(7) language support environment  running in alpine include nginx php7!
rsync           ---> an open source utility that provides fast incremental file transfer. 
sshd            ---> support remote access via ssh ,running in debian buster
ttyd            ---> Share your terminal over the web
```
